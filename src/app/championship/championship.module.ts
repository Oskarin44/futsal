import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChampionshipRoutingModule } from './championship-routing.module';
import { ChampionshipListComponent } from './championship-list/championship-list.component';

@NgModule({
  declarations: [ChampionshipListComponent],
  imports: [
    CommonModule,
    ChampionshipRoutingModule,
    SharedModule
  ]
})
export class ChampionshipModule { }
