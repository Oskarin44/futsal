import { League } from './../models/league.model';
import { Championship } from './../models/championship.model';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-form-modal',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  public championship =  new Championship("");
  public isModalHidden: boolean = true;
  champForm: FormGroup;
  league: League;

  constructor() {
  }

  ngOnInit() {
    this.createFormController();
    this.championship = new Championship("");
  }

  createFormController() {
    this.champForm = new FormGroup({
      'name': new FormControl('', [Validators.required, Validators.maxLength(20)])
    });
  }

  showModal(league){
    this.league = league;
    this.championship = new Championship("");
    this.isModalHidden = false;
  }

  save(){
    this.league.addChampionship(this.championship);
    this.isModalHidden = true;
  }

  hideModal(){
    this.isModalHidden = true;
  }

}
