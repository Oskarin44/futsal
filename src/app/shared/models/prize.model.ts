import { Team } from './team.model';
import { Player } from './player.model';
import { Match } from './match.model';
export class Prize {
      first: Team;
      second: Team;
      third: Team;
      firstShooter: any;
      secondShooter: any;
      thirdShooter: any;
      goldAx: any;
      plateAx: any;
      bronzeAx: any;
      aux: any[];
      allShooters: any[];
      allRedCards: any[];

      constructor(){
            this.first = null,
            this.second = null,
            this.third = null,
            this.firstShooter = null,
            this.secondShooter = null,
            this.thirdShooter = null,
            this.goldAx = null,
            this.plateAx = null,
            this.bronzeAx = null,
            this.aux = [];
      }

      getBest(matches: Match[]) {
            let aux = [];
            for (let i = 0; i < matches.length; i++) {     
                   aux.push(matches[i].getWinner());
            }
            this.getOccurrence(aux);
            this.setBest();
      }

      getOccurrence(array) {
            this.aux = [];
            for (let i = 0; i < array.length; i++) {
                  let count = 0;
                  for (let j = i; j < array.length; j++) {
                        if (array[i] === array[j]) {
                              count = count+1;
                        }
                  }
                  this.aux.push([array[i],count]);
            }
      }
      
      setBest() {
            this.aux.sort();
            this.first = this.aux[6];
            this.getSecond();
            this.getThird();
      }

      getThird() {
            let cont = 5;
            while (cont !== 0) {
                  if ((this.aux[cont][0].name !== this.first[0].name) && (this.aux[cont][0].name !== this.second[0].name)) {
                        this.third = this.aux[cont];
                        cont = 0;
                  } else {
                        cont = cont - 1;
                  }
            }
      }
      
      getSecond() {
            let cont = 6;
            while (cont !== 0) {
                  if (this.aux[cont][0].name !== this.first[0].name) {
                        this.second = this.aux[cont];
                        cont = 0;
                  } else {
                        cont = cont - 1;
                  }
            }
      }

      setPlaces(matches: Match[]) {
            this.allShooters = [];
            this.allRedCards = [];
            for (let i = 0; i < matches.length; i++) {
                  this.countShooters(matches[i].getShooters());    
                  this.countRedCards(matches[i].getRedCards());
            }
            this.getShooterPlaces();
            this.getWorstPlaces();
      }

      getShooterPlaces() {
            this.allShooters.sort();
            this.firstShooter = this.allShooters.slice(-1);
            this.allShooters.pop();
            this.secondShooter = this.allShooters.slice(-1);
            this.allShooters.pop();
             this.thirdShooter = this.allShooters.slice(-1);
            this.allShooters.pop();
      }

      getWorstPlaces() {
            this.allRedCards.sort();
            this.goldAx = this.allRedCards.slice(-1);
            this.allRedCards.pop();
            this.plateAx = this.allRedCards.slice(-1);
            this.allRedCards.pop();
            this.bronzeAx = this.allRedCards.slice(-1);
            this.allRedCards.pop();
      }

      countShooters(shootersList) {
            for (let i = 0; i < shootersList.length; i++) {
                  let isNotIn = this.isNotInShootersList(shootersList[i][0]);
                  if (isNotIn) {
                        this.allShooters.push([shootersList[i][0],shootersList[i][1]]);  
                  } else {
                        this.incrementGoal(shootersList[i][0],shootersList[i][1]);
                  }
            }
      }

      isNotInShootersList(player: Player) {
            if (this.allShooters.length === 0) {
                  return true;
            } else {
                  return this.isInList(this.allShooters, player);
            }
      }

      incrementGoal(player: Player, goals) {
            for (let i = 0; i < this.allShooters.length; i++) {
                  if (this.allShooters[i][0].id === player.id) {
                        this.allShooters[i][1] = this.allShooters[i][1] + goals;
                        break;
                  }
            }
      }

      countRedCards(redCardslist) {
            for (let i = 0; i < redCardslist.length; i++) {
                  let isNotIn = this.isNotInRedList(redCardslist[i][0]);
                  if (isNotIn) {
                        this.allRedCards.push([redCardslist[i][0],redCardslist[i][1]]);  
                  } else {
                        this.incrementRed(redCardslist[i][0],redCardslist[i][1]);
                  }
            }
      }

      isNotInRedList(player: Player) {
            if (this.allRedCards.length === 0) {
                  return true;
            } else {
                  return this.isInList(this.allRedCards, player);
            }
      }

      incrementRed(player: Player, redCards) {
            for (let i = 0; i < this.allRedCards.length; i++) {
                  if (this.allRedCards[i][0].id === player.id) {
                        this.allRedCards[i][1] = this.allRedCards[i][1] + redCards;
                        break;
                  }
            }
      }

      isInList(list, item) {
            let resp = true;
            for (let i = 0; i < list.length; i++) {
                  if (list[i][0] === item) {
                        resp = false;
                        break;
                  }
            }
            return resp;
      }

      


}