import { Player } from './player.model';
export class Information {

      player: Player;
      goals: number;
      assists: number;
      yellowCard: number;
      redCard: number;

      constructor(player,goals,assist,yellow,red) {
            this.player = player;
            this.goals = goals ;
            this.assists = assist;
            this.yellowCard = yellow;
            this.redCard = red;
      }

}