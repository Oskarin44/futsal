import { Match } from './match.model';
import { Player } from './player.model';
import { Team } from './team.model';
import { Championship } from './championship.model';
export class League {
      name: String;
      championships: Championship[];
      constructor(name) {
            this.name = name;
            this.championships = [];
      }

      addChampionship(championship: Championship){
            this.championships.push(championship);
      }

      addTeam(pos, team:Team) {
            this.championships[pos].teams.push(team);
      }

      addPlayer(posC, posP, player:Player) {
            if (this.championships[posC].teams[posP].players.length < 10) {
                  this.championships[posC].teams[posP].players.push(player);
            }
      }

      registerMatch(pos, match:Match) {
            this.championships[pos].addMatch(match);
      }
}